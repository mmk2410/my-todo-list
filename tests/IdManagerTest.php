<?php declare(strict_types=1);


use MMK2410\MyTodoList\IdManager;
use PHPUnit\Framework\TestCase;

class IdManagerTest extends TestCase
{
    public function testGenerateTwoIds(): void
    {
        $id1 = IdManager::generateID(IdManagerTest::class . "::testGenerateTwoIds");
        $id2 = IdManager::generateID(IdManagerTest::class . "::testGenerateTwoIds");
        self::assertNotEquals(
            $id1,
            $id2,
            "ID 1 '$id1' and ID 2 '$id2' are equal, but the shouldn't."
        );
    }

    public function testGetCurrentId(): void
    {
        for ($i = 0; $i < 3; $i++) {
            IdManager::generateID(IdManagerTest::class . "::testGetCurrentId()");
        }
        $currentId = IdManager::getCurrentId(IdManagerTest::class . "::testGetCurrentId()");
        self::assertEquals(
            2,
            $currentId,
            "Expected current ID to be '2', but got '$currentId'"
        );
    }
}
