<?php declare(strict_types=1);

use MMK2410\MyTodoList\Todo;
use MMK2410\MyTodoList\TodoStates;
use PHPUnit\Framework\TestCase;

class TodoTest extends TestCase
{
    public function testCreateTodo(): void {
        $todo1 = new Todo("Some title");
        $todo2 = new Todo("Some title");
        $this->assertNotEquals(
            $todo1->getID(),
            $todo2->getID(),
            "Expected todos to have different UIDs but they have the same."
        );
    }

    public function testTodoHasTitle(): void
    {
        $title = "Some task";
        $todo = new Todo($title);
        $this->assertEquals(
            $title,
            $todo->getTitle(),
            "Expected title '$title', but got '{$todo->getTitle()}'"
        );
    }

    public function testDontCreateEmptyTodo(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(Todo::ExceptionMsgInvalidTitle);
        new Todo("");
    }

    public function testUpdateTitle(): void
    {
        $anotherTitle = "Another title";
        $todo = new Todo("Some title");
        $todo->setTitle($anotherTitle);
        $this->assertEquals(
            $anotherTitle,
            $todo->getTitle(),
            "Expected new title $anotherTitle, but got {$todo->getTitle()}"
        );
    }

    public function testNewTodoState(): void
    {
        $todo = new Todo("stub");
        $this->assertEquals(
            TodoStates::Todo,
            $todo->getStatus(),
            "Expected todo state 'Todo', but got {$todo->getStatus()}"
        );
    }

    public function testChangeTodoState(): void
    {
        $todo = new Todo("stub");
        $todo->setState(TodoStates::Done);
        $this->assertEquals(
            TodoStates::Done,
            $todo->getStatus(),
            "Expected todo state 'Done', but got {$todo->getStatus()}"
        );
    }

    public function testSetBogusTodoState(): void
    {
       $todo = new Todo("stub");
       $this->expectException(InvalidArgumentException::class);
       $this->expectExceptionMessage(Todo::ExceptionMsgInvalidState);
       $todo->setState("bogus");
    }

    public function testCreateImportantTodo(): void
    {
        $todo = new Todo("Some title", true);
        $this->assertTrue(
            $todo->getImportant(),
            "Todo created as important, but it is not"
        );
    }

    public function testMakeTodoImportant(): void {
        $todo = new Todo("Some title");
        $todo->setImportant();
        $this->assertTrue(
            $todo->getImportant(),
            "Todo changed to important, but it is not"
        );
    }
}
