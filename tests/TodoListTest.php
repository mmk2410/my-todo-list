<?php declare(strict_types=1);

use MMK2410\MyTodoList\Todo;
use MMK2410\MyTodoList\TodoList;
use PHPUnit\Framework\TestCase;

class TodoListTest extends TestCase
{
    public function testCreateTodoList(): void {
        $todolist1 = new TodoList("My Todo List");
        $todolist2 = new TodoList("My Todo List");
        $this->assertNotEquals(
            $todolist1->getID(),
            $todolist2->getID(),
            "Expected todo lists to have different UIDs but they have the same."
        );
    }

    public function testTodoListHasName(): void
    {
        $name = "My Todo List";
        $todolist = new TodoList($name);
        $this->assertEquals(
            $name,
            $todolist->getName(),
            "Created todo list with name '$name', but it has name '{$todolist->getName()}'",
        );
    }

    public function testChangeTodoListName(): void
    {
        $todolist = new TodoList("Old name");
        $new_todolist_name = "New name";
        $todolist->setName($new_todolist_name);
        $this->assertEquals(
            $new_todolist_name,
            $todolist->getName(),
            "Renamed todo list to '$new_todolist_name', but its name is '{$todolist->getName()}'"
        );
    }

    public function testGetEmptyTodos(): void
    {
        $todolist = new TodoList("Some list");
        $todoAmount = sizeof($todolist->getTodos());
        $this->assertEquals(
            0,
            $todoAmount,
            "Got wrong amount of todos. Expected 0, got $todoAmount"
        );
    }

    public function testGetTodos(): void
    {
        $todolist = new TodoList("Some list");
        for ($i = 1; $i < 3; $i++) {
            $todolist->addTodo(new Todo("Some todo"));
        }
        $i--; // Decrement $i for using it in the assertion
        $todoAmount = sizeof($todolist->getTodos());
        $this->assertEquals(
            $i,
            $todoAmount,
            "Got wrong amount of todos. Expected $i, got $todoAmount"
        );
    }

    public function testAddTodo(): void
    {
        $todolist = new TodoList("Some list");
        $todo = new Todo("Some todo");
        $todolist->addTodo($todo);
        $id_todo = $todo->getID();
        $id_todo_from_list = $todolist->getTodos()[0]->getID();
        $this->assertEquals(
            $id_todo,
            $id_todo_from_list,
            "Got wrong todo. Expected ID $id_todo, got $id_todo_from_list"
        );
    }

    public function testGetTodoById(): void
    {
        $todolist = new TodoList("Some list");
        $todo = new Todo("Some todo");
        $todolist->addTodo($todo);
        $id_todo = $todo->getID();
        $id_todo_from_list = $todolist->getTodoById($id_todo)->getID();
        $this->assertEquals(
            $id_todo,
            $id_todo_from_list,
            "Got wrong todo. Expected ID $id_todo, got $id_todo_from_list"
        );
    }

    public function testGetNonExistentTodoById(): void
    {
        $todolist = new TodoList("Some list");
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage(TodoList::ExceptionMsgTodoNotFound);
        $todolist->getTodoById(-1);
    }

    public function testDeleteTodoById(): void
    {
        $todolist = new TodoList("Some list");
        $todo = new Todo("Delete me");
        $todolist->addTodo($todo);
        $todolist->deleteTodoById($todo->getID());
        $amountTodos = sizeof($todolist->getTodos());
        $this->assertEquals(
            0,
            $amountTodos,
            "Expected not todos to be left, but there are/is $amountTodos left"
        );
    }
}
