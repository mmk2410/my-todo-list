# My ToDo List

My todo list is a PHP project that I develop for training PHP.

## Development

Currently, I use PHP 7.4 (maybe I will switch to PHP 8 soon) and follow a TDD approach.
The program will store its data in a MySQL database that will be connected and accessed using the Doctrine DBAL.
Goal of the PHP application is a RESTful API build with some web framework like Symphony or Laravel.
This will be used by a frontend that won't be powered by PHP but (as of now) TypeScript and some fancy JavaScript framework like Angular, React or Vue.