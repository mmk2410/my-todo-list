<?php

namespace MMK2410\MyTodoList;

abstract class TodoStates
{
    const Todo = "Todo";
    const Done = "Done";
}