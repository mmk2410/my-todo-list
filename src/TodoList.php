<?php

namespace MMK2410\MyTodoList;

use InvalidArgumentException;

class TodoList
{
    const ExceptionMsgTodoNotFound = "Could not find todo with given ID";

    private string $name;
    private array $todos;
    private int $id;

    public function __construct(string $name)
    {
        $this->id = IdManager::generateID(self::class);
        $this->name = $name;
        $this->todos = array();
    }

    public function getID(): int {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function addTodo(Todo $todo): void
    {
        $this->todos[] = $todo;
    }

    public function getTodos(): array
    {
        return $this->todos;
    }

    public function getTodoById(int $id): Todo
    {
        foreach ($this->todos as $todo) {
            if ($todo->getID() == $id) {
                return $todo;
            }
        }
        throw new InvalidArgumentException(self::ExceptionMsgTodoNotFound);
    }

    public function deleteTodoById(int $id): void
    {
        foreach ($this->todos as $key => $todo) {
            if ($todo->getID() == $id) {
                unset($this->todos[$key]);
            }
        }
    }
}