<?php declare(strict_types=1);

namespace MMK2410\MyTodoList;

class IdManager
{
    private static array $ids = array();

    public static function generateID(string $classname): int
    {
        if (!array_key_exists($classname, self::$ids)) {
            self::$ids[$classname] = 0;
            return 0;
        } else {
            return ++self::$ids[$classname];
        }
    }

    public static function getCurrentId(string $classname)
    {
        if (!array_key_exists($classname, self::$ids)) {
            return 0;
        } else {
            return self::$ids[$classname];
        }
    }
}