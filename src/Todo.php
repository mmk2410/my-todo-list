<?php declare(strict_types=1);

namespace MMK2410\MyTodoList;

use InvalidArgumentException;
use ReflectionClass;

class Todo
{
    const ExceptionMsgInvalidTitle = "Empty task title not allowed";
    const ExceptionMsgInvalidState = "Invalid state tried to set.";

    private string $title;
    private string $state;
    private bool $important;
    private int $id;

    public function __construct(string $title, bool $important = false)
    {
        $this->validateTitle($title);
        $this->id = IdManager::generateID(self::class);
        $this->title = $title;
        $this->state = TodoStates::Todo;
        $this->important = $important;
    }

    public function getID(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->validateTitle($title);
        $this->title = $title;
    }

    public function getStatus(): string
    {
        return $this->state;
    }

    public function setState(string $state): void
    {
        $this->validateState($state);
        $this->state = $state;
    }

    public function getImportant(): bool {
        return $this->important;
    }

    public function setImportant(): void {
        $this->important = true;
    }

    private function validateTitle(string $title): void
    {
        if (empty($title)) {
            throw new InvalidArgumentException(self::ExceptionMsgInvalidTitle);
        }
    }

    private function validateState(string $state): void
    {
        $reflect = new ReflectionClass(TodoStates::class);
        $constantFound = $reflect->getConstant($state);
        if ($constantFound === FALSE) {
            throw new InvalidArgumentException(self::ExceptionMsgInvalidState);
        }
    }
}